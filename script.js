const tabs = document.querySelector(".tabs");
const tabsItem = document.querySelectorAll("ul.tabs-content > li");

tabs.addEventListener("click", (event) => {
  const tabsTitleActive = document.querySelector(".active");  //declare and find an element with a class 'active'
  tabsTitleActive.classList.remove("active");                 //remove the class 'active'  from the found element
  const tabItem = event.target.innerHTML;                     //declare the variable that was the event object
  event.target.classList.add("active");                       //assigning a class 'active' to the event object
  hidingItems(tabsItem, tabItem);
});

function hidingItems(arr, item) {
  arr.forEach((element) => {
    //hide the contents of all elements by assigning a value to their attribute, and display the contents of the selected element
    element.hidden = true;    
    if (item === element.dataset.name) element.hidden = false;
  });
}
